FROM rekgrpth/libv8
RUN exec 2>&1 \
    && set -ex \
    && apk add --no-cache --virtual .build-deps \
        g++ \
        git \
        linux-headers \
        make \
    && JUST=/tmp/just \
    && git clone --recursive https://github.com/RekGRpth/just.git $JUST \
    && git clone --recursive https://github.com/RekGRpth/modules.git $JUST/modules \
    && mkdir -p $JUST/deps $JUST/modules/picohttp/deps \
    && git clone --recursive https://github.com/RekGRpth/picohttpparser.git $JUST/modules/picohttp/deps/picohttpparser \
    && cd $JUST \
    && make -j"$(nproc)" install \
    && JUST_HOME=$(pwd) make -j"$(nproc)" -C modules/html install \
    && JUST_HOME=$(pwd) make -j"$(nproc)" -C modules/picohttp install \
    && (strip /usr/local/bin/* /usr/local/lib/* || true) \
    && apk add --no-cache --virtual .just-rundeps \
        $(scanelf --needed --nobanner --format '%n#p' --recursive /usr/local | tr ',' '\n' | sort -u | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }') \
    && apk del --no-cache .build-deps \
    && rm -rf /usr/share/doc /usr/share/man /usr/local/share/doc /usr/local/share/man /tmp/* /var/tmp/* /var/cache/apk/* \
    && echo done
